﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace DAL.Context
{
    public class HTTPRequest<T> where T : class
    {
        public List<T> Get(HttpClient client, string url)
        {
            List<T> output = null;

            var task = client.GetAsync(url).ContinueWith((response) =>
            {
                  var responseMessage = response.Result;
                  var jsonString = responseMessage.Content.ReadAsStringAsync();
                  jsonString.Wait();
                  output = JsonConvert.DeserializeObject<List<T>>(jsonString.Result);
            });
            
            task.Wait();

            return output;
        }
    }
}
