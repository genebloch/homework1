﻿using DAL.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace DAL.Context
{
    public class DataContext
    {
        private readonly string _base;
        private readonly HttpClient _client;

        public List<User> Users => new HTTPRequest<User>().Get(_client, $"{_base}/Users");
        public List<Team> Teams => new HTTPRequest<Team>().Get(_client, $"{_base}/Teams");
        public List<Project> Projects => new HTTPRequest<Project>().Get(_client, $"{_base}/Projects");
        public List<Task> Tasks => new HTTPRequest<Task>().Get(_client, $"{_base}/Tasks");
        public List<TaskState> States => new HTTPRequest<TaskState>().Get(_client, $"{_base}/TaskStates");

        public DataContext()
        {
            _base = "https://bsa20.azurewebsites.net/api";
            _client = new HttpClient();
        }
    }
}
