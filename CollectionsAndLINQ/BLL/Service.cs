﻿using DAL.Context;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class Service
    {
        private readonly DataContext _db;

        public Service()
        {
            _db = new DataContext();
        }

        public Dictionary<Project, int> GetUserTasksInProject(int userId)
        {
            return _db.Projects.Where(p => p.AuthorId == userId).Select(p => new
            {
                project = p,
                tasksAmount = _db.Tasks.Count(task => task.ProjectId == p.Id)
            }).ToDictionary(x => x.project, x => x.tasksAmount);
        }

        public List<Task> GetUserTasksWithLength(int userId, int maxTaskNameLength)
        {
            return _db.Tasks.Where(t => t.PerformerId == userId)
                .Where(t => t.Name.Length < maxTaskNameLength).ToList();
        }

        public List<(int id, string name)> GetUserFinishedTasks(int userId, int finishedAtYear)
        {
            return _db.Tasks.Where(t => t.PerformerId == userId)
                .Where(t => t.FinishedAt.Year == finishedAtYear)
                .Select(t => (t.Id, t.Name)).ToList();
        }

        public List<User> GetUsers(int minAge)
        {
            return _db.Users.Where(u => DateTime.Now.Year - u.Birthday.Year > minAge)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .SelectMany(u => u).ToList();
        }

        public List<(User user, List<Task> tasks)> GetSortedUsersWithTasks()
        {
            var output = new List<(User, List<Task>)>();

            var query = _db.Users.OrderBy(u => u.FirstName).Select(u => new
            {
                user = u,
                tasks = _db.Tasks.Where(t => t.PerformerId == u.Id).OrderByDescending(t => t.Name.Length).ToList()
            }).ToList();

            foreach (var obj in query) output.Add((obj.user, obj.tasks));

            return output;
        }

        public (Project last, int tasksAmount, int notFinishedTasks, Task theLongest) GetUserInfo(int userId)
        {
            var query = new
            {
                last = _db.Projects.Where(project => project.AuthorId == userId).OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                tasksAmount = -1, //Загальна кількість тасків під останнім проектом
                notFinishedTasks = _db.Tasks.Where(t => t.PerformerId == userId).Count(t => t.State == 2 || t.State == 3),
                theLongest = _db.Tasks.Where(t => t.PerformerId == userId).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
            };

            return (query.last, query.tasksAmount, query.notFinishedTasks, query.theLongest);
        }

        public List<(Project project, Task theLongestDescription, Task theShortestName, int usersAmount)> GetProjectsInfo() 
        {
            var output = new List<(Project, Task, Task, int)>();

            var query = _db.Projects.Select(p => new 
            {
                project = p,
                theLongestDescription = _db.Tasks.Where(t => t.ProjectId == p.Id).OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                theShortestName = _db.Tasks.Where(t => t.ProjectId == p.Id).OrderBy(t => t.Name.Length).FirstOrDefault(),
                usersAmount = -1 //Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
            }).ToList();

            foreach (var obj in query) output.Add((obj.project, obj.theLongestDescription, obj.theShortestName, obj.usersAmount));

            return output;
        }
    }
}
