﻿using BLL;

namespace API
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new Service();

            var output1 = service.GetUserTasksInProject(7);
            var output2 = service.GetUserTasksWithLength(7, 45); 
            var output3 = service.GetUserFinishedTasks(7, 2020);
            var output4 = service.GetUsers(10);
            var output5 = service.GetSortedUsersWithTasks();
            var output6 = service.GetUserInfo(7);
            var output7 = service.GetProjectsInfo();
        }
    }
}
